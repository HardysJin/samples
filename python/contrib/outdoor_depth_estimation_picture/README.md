**This sample provides reference for you to learn the Ascend AI Software Stack and cannot be used for commercial purposes.**

This sample works with CANN 3.0.0 and later versions, and supports Atlas 200 DK and Atlas 300.

# Outdoor Depth Estimation - FeatDepth
**Function**: Estimate Depth Info from single RGB image

**Input**: a source RGB image
![input](https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/indoor_depth_estimation_picture/test.jpg)

**Output**: the Depth image
![output](https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/indoor_depth_estimation_picture/verify.jpg)

**Border Issue**: because `atc` conversion does not support reflect padding, thus changed to zero padding, which should be fixed in future version.

## Prerequisites

Before deploying this sample, ensure that:

- The environment has been set up by referring to [Environment Preparation and Dependency Installation](https://gitee.com/ascend/samples/blob/master/python/environment/README.md).
- The development environment and operating environment of the corresponding product have been set up.

## Software Preparation
* Make sure you log in to the operating environment (**HwHiAiUser**)
    ```
    ssh HwHiAiUser@xxx.xxx.xxx.xxx
    ```
    ![Icon-note.gif](https://images.gitee.com/uploads/images/2020/1106/160652_6146f6a4_5395865.gif) **NOTE**

    > Replace ***xxx.xxx.xxx.xxx*** with the IP address of the operating environment. The IP address of Atlas 200 DK is **192.168.1.2** when it is connected over the USB port, and that of Atlas 300 is the corresponding public network IP address.

### 1. Obtain the source package.
```
cd $HOME
git clone https://gitee.com/ascend/samples.git
```

### 2. Obtain the Offline Model (**om**) or Convert **pb** to **om** in [Step 3](#3-(OPTIONAL)-Convert-the-original-pb-model-to-a-DaVinci-offline-model.).

   Ensure you are in the project directory (`outdoor_depth_estimation_picture/`) and run one of the following commands in the table to obtain the pedestrian tracking model used in the application.

	cd $HOME/samples/python/contrib/outdoor_depth_estimation_picture/

| **Model**  |  **How to Obtain** |
| ---------- |  ----------------- |
| outdoor_depth_esti_featdepth.om | `wget -nc --no-check-certificate 'https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/outdoor_depth_estimation_picture/outdoor_depth_esti_featdepth.om' -O model/outdoor_depth_esti_featdepth.om`  |
| outdoor_depth_esti_featdepth.onnx | `wget -nc --no-check-certificate 'https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/outdoor_depth_estimation_picture/outdoor_depth_esti_featdepth.onnx' -O model/outdoor_depth_esti_featdepth.onnx`  |

   ![Icon-note.gif](https://images.gitee.com/uploads/images/2020/1106/160652_6146f6a4_5395865.gif) **NOTE**
   >- `outdoor_depth_esti_featdepth.om` offline model you can use out-of-the-box without model conversion. If you use this then you can skip the next step on model conversion.
   >- `outdoor_depth_esti_featdepth.onnx` onnx model for those that want to configure the model conversion process.

### 3. **(OPTIONAL)** Convert the original pb model to a DaVinci offline model.

   **Note: Ensure that the environment variables have been configured in [Environment Preparation and Dependency Installation](https://gitee.com/ascend/samples/tree/master/python/environment).**

   1. Set the ***LD_LIBRARY_PATH*** environment variable.

      The ***LD_LIBRARY_PATH*** environment variable conflicts with the sample when Ascend Tensor Compiler (ATC) is used. Therefore, you need to set this environment variable separately in the command line to facilitate modification.
      
          export LD_LIBRARY_PATH=${install_path}/atc/lib64

   For **CANN 3.0.0 and later**: <br/>

   2. Go to the project directory (outdoor_depth_estimation_picture) and run the model conversion command to convert the model:

          cd $HOME/samples/python/contrib/outdoor_depth_estimation_picture/
          atc --input_shape="input:1,3,320,1024" --input_format=NCHW --model model/outdoor_depth_esti_featdepth.onnx --output model/outdoor_depth_esti_featdepth --soc_version=Ascend310 --framework=5 


## Sample Running
   - ### Test Sample Image
     ```
     cd $HOME/samples/python/contrib/outdoor_depth_estimation_picture/src
     python3 main.py
     ```
     Check `test_output.jpg` in `output` directory
