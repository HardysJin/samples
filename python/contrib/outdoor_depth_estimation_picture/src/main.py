"""
Copyright 2021 Huawei Technologies Co., Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np
import cv2
import argparse
import sys
import os
import matplotlib.pyplot as plt

sys.path.append("../../../common/")

from atlas_utils.acl_model import Model
from atlas_utils.acl_resource import AclResource 

def disp_to_depth(disp, min_depth=0.1, max_depth=100):
    min_disp = 1 / max_depth # 0.1
    max_disp = 1 / min_depth # 100
    scaled_disp = min_disp + (max_disp - min_disp) * disp #(100 - 0.1) * disp + 0.1
    depth = 1 / scaled_disp
    return scaled_disp, depth

def main(): 
    """main"""

    description = 'Outdoor Depth Estimation'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input_image', type=str, default='../data/test.jpg', help="Path for input image")
    args = parser.parse_args()

    #initialize acl runtime 
    acl_resource = AclResource()
    acl_resource.init()

    model = Model("../model/outdoor_depth_esti_featdepth.om")

    #read image & pre process
    img = (cv2.imread(args.input_image) / 255.).astype(np.float32)
    img = cv2.resize(img, (1024, 320))
    img = np.transpose(img, (2, 0, 1))
    img = np.expand_dims(img, axis=0)

    # execute
    out = model.execute(img.copy())

    # post process
    pred_disp, _ = disp_to_depth(out[0], 0.1, 100)
    pred_disp = pred_disp.squeeze()
    vmax = np.percentile(pred_disp, 95)

    out_dir = "../output/"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    plt.imsave(os.path.join(out_dir, "test_output.jpg"), pred_disp, cmap='magma', vmax=vmax)


if __name__ == "__main__":
    main()