**This sample provides reference for you to learn the Ascend AI Software Stack and cannot be used for commercial purposes.**

This sample works with CANN 3.0.0 and later versions, and supports Atlas 200 DK and Atlas 300.

# Indoor Depth Estimation - AdaBins
**Function**: Estimate Depth Info from single RGB image

**Input**: a source RGB image
<img src="https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/indoor_depth_estimation_picture/test.jpg" width="325"/>

**Output**: the Depth image
![output](https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/indoor_depth_estimation_picture/verify.jpg)

## Prerequisites

Before deploying this sample, ensure that:

- The environment has been set up by referring to [Environment Preparation and Dependency Installation](https://gitee.com/ascend/samples/blob/master/python/environment/README.md).
- The development environment and operating environment of the corresponding product have been set up.

## Software Preparation
* Make sure you log in to the operating environment (**HwHiAiUser**)
    ```
    ssh HwHiAiUser@xxx.xxx.xxx.xxx
    ```
    ![Icon-note.gif](https://images.gitee.com/uploads/images/2020/1106/160652_6146f6a4_5395865.gif) **NOTE**

    > Replace ***xxx.xxx.xxx.xxx*** with the IP address of the operating environment. The IP address of Atlas 200 DK is **192.168.1.2** when it is connected over the USB port, and that of Atlas 300 is the corresponding public network IP address.

### 1. Obtain the source package.
```
cd $HOME
git clone https://gitee.com/ascend/samples.git
```

### 2. Obtain the Offline Model (**om**) or Convert **pb** to **om** in [Step 3](#3-(OPTIONAL)-Convert-the-original-pb-model-to-a-DaVinci-offline-model.).

   Ensure you are in the project directory (`indoor_depth_estimation_picture/`) and run one of the following commands in the table to obtain the pedestrian tracking model used in the application.

	cd $HOME/samples/python/contrib/ouinor_depth_estimation_picture/

| **Model**  |  **How to Obtain** |
| ---------- |  ----------------- |
| indoor_depth_esti_adabins.om | `wget -nc --no-check-certificate 'https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/ouinor_depth_estimation_picture/indoor_depth_esti_adabins.om' -O model/indoor_depth_esti_adabins.om`  |
| indoor_depth_esti_adabins.onnx | `wget -nc --no-check-certificate 'https://modelzoo-train-atc.obs.cn-north-4.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/ouinor_depth_estimation_picture/indoor_depth_esti_adabins.onnx' -O model/indoor_depth_esti_adabins.onnx`  |

   ![Icon-note.gif](https://images.gitee.com/uploads/images/2020/1106/160652_6146f6a4_5395865.gif) **NOTE**
   >- `indoor_depth_esti_adabins.om` offline model you can use out-of-the-box without model conversion. If you use this then you can skip the next step on model conversion.
   >- `indoor_depth_esti_adabins.onnx` onnx model for those that want to configure the model conversion process.

### 3. **(OPTIONAL)** Convert the original pb model to a DaVinci offline model.

   **Note: Ensure that the environment variables have been configured in [Environment Preparation and Dependency Installation](https://gitee.com/ascend/samples/tree/master/python/environment).**

   1. Set the ***LD_LIBRARY_PATH*** environment variable.

      The ***LD_LIBRARY_PATH*** environment variable conflicts with the sample when Ascend Tensor Compiler (ATC) is used. Therefore, you need to set this environment variable separately in the command line to facilitate modification.
      
          export LD_LIBRARY_PATH=${install_path}/atc/lib64

   For **CANN 3.0.0 and later**: <br/>

   2. Go to the project directory (head_pose_picture) and run the model conversion command to convert the model:
   
          cd $HOME/samples/python/contrib/head_pose_picture/
          atc --input_shape="input.1:1,3,480,640" --input_format=NCHW --model model/indoor_depth_esti_adabins.onnx --output model/indoor_depth_esti_adabins --soc_version=Ascend310 --framework=5 


## Sample Running
   - ### Test Sample Image
     ```
     cd $HOME/samples/python/contrib/head_pose_picture/src
     python3 main.py --input_image ../data/test.jpg
     ```
     Check `test_output.jpg` in `output` directory
