"""
Copyright 2021 Huawei Technologies Co., Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np
import cv2
import argparse
import sys, os
import matplotlib.pyplot as plt

sys.path.append("../../../common/")

from atlas_utils.acl_model import Model
from atlas_utils.acl_resource import AclResource 

def pre_process(image):
    image = cv2.resize(image, (640, 480), interpolation = cv2.INTER_AREA)
    image = image.transpose((2, 0, 1))
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    for channel in range(3):
        image[channel] = (image[channel] - mean[channel]) / std[channel]
    return np.expand_dims(image, axis=0)
    
def main(): 
    """main"""
    description = 'Indoor Depth Estimation'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input_image', type=str, default='../data/test.jpg', help="Path for input image")
    args = parser.parse_args()

    #initialize acl runtime 
    acl_resource = AclResource()
    acl_resource.init()

    # load model
    model = Model("../model/indoor_depth_esti_adabins.om")

    # load image
    img = cv2.imread(args.input_image) / 255.
    
    # pre process
    processed = pre_process(img).astype(np.float32).copy()

    # execute
    out = model.execute(processed)

    # post process
    min_depth = 1e-3
    max_depth = 10
    final = np.clip(out[0], min_depth, max_depth)

    final[final < min_depth] = min_depth
    final[final > max_depth] = max_depth
    final[np.isinf(final)] = max_depth
    final[np.isnan(final)] = min_depth


    out_dir = "../output/"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    plt.imsave(os.path.join(out_dir, "test_output.jpg"), final.squeeze())

if __name__ == "__main__":
    main()